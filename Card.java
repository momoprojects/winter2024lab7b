public class Card {
    private Suit suit;
    private Rank rank;

    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public Suit getSuit() {
        return this.suit;
    }

    public Rank getRank() {
        return this.rank;
    }

    public String toString() {
        String nameOfRank = this.rank.toString();
        nameOfRank= nameOfRank.substring(0,1).toUpperCase() + nameOfRank.substring(1).toLowerCase();

        String nameOfSuit= this.suit.toString();
        nameOfSuit=nameOfSuit.substring(0,1).toUpperCase() + nameOfSuit.substring(1).toLowerCase();
        return nameOfRank + " of " + nameOfSuit;
    }
}
