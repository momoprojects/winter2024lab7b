public class SimpleWar {

    public static void main(String[] args){
        int playerOneScore=0;
        int playerTwoScore=0;
        int roundOfMatch=1;
        Deck deck= new Deck();
        while(deck.deckLength()>1){
            deck.shuffle();
            Card cardOfPlayerOne= deck.drawTopCard();
            Card cardOfPlayerTwo= deck.drawTopCard();
            System.out.println("Round: "+roundOfMatch);
            System.out.println("Score:\n    Player One:"+playerOneScore+"\n    Player Two:"+ playerTwoScore);
            System.out.println("\nCard of Player One: "+cardOfPlayerOne+" VS Card of Player Two: "+cardOfPlayerTwo);
            double scoreOfCardOne=calculateScore(cardOfPlayerOne);
            double scoreOfCardTwo=calculateScore(cardOfPlayerTwo);
            System.out.println("\nDetermined power of Cards: "+scoreOfCardOne+" VS "+scoreOfCardTwo);
            Card winnerCard=null;
            if (scoreOfCardOne>scoreOfCardTwo){
            winnerCard= cardOfPlayerOne;
                playerOneScore++;
            }
            else{
                winnerCard=cardOfPlayerTwo;
                playerTwoScore++;
            }
            System.out.println("Winner is: "+winnerCard);
            System.out.println("Score:\n    Player One:"+playerOneScore+"\n    Player Two:"+ playerTwoScore+"\n\n==============================\n");
            roundOfMatch++;
        }
        String winnerOfMatch="";
        if (playerOneScore>playerTwoScore) winnerOfMatch="Player One";
        else if (playerOneScore<playerTwoScore) winnerOfMatch="Player Two";
        else winnerOfMatch="Nobody, because it was a tie";
        System.out.println("Congratulations to "+winnerOfMatch+"! Final score was "+playerOneScore+" and "+playerTwoScore);
    }
    public static double calculateScore(Card card){
        double rankOfCard=card.getRank().getScore();
        double suitOfCard= card.getSuit().getScore();
        return rankOfCard+suitOfCard;

    }
}
