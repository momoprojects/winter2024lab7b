import java.util.Random;

public class Deck {
    private Card[] cards;
    private int numOfCards;
    private Random rng;

    public Deck() {
        System.out.println("\nCreating new deck...\n");
        this.numOfCards = Suit.values().length * Rank.values().length;
        rng = new Random();
        cards = new Card[this.numOfCards];
        Suit[] listOfSuits = Suit.values();
        Rank[] listOfRanks = Rank.values();
        int i = 0;
        for (Suit s : listOfSuits) {
            for (Rank r : listOfRanks) {
                cards[i] = new Card(s, r);
                i++;
            }
        }
    }

    public String toString() {
        // another way to print entire array
        // return Arrays.deepToString(cards);
        System.out.println("\nPrinting deck...");
        String output = "";
        for (int i = 0; i < this.numOfCards; i++) {
            output += cards[i] + "\n";
        }
        return output;
    }

    public int deckLength() {
        return this.numOfCards;
    }

    public Card drawTopCard() {
        this.numOfCards--;
        Card lastCard = cards[numOfCards];
        cards[numOfCards] = null;
        return lastCard;
    }

    public void shuffle() {
        for (int i = 0; i < this.numOfCards; i++) {
            Card temp = this.cards[i];
            int randomIndex = rng.nextInt(this.numOfCards);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }

    public void removeFromNumOfCards(int numberOfCardsToBeTaken) {
        this.numOfCards -= numberOfCardsToBeTaken;
    }

}