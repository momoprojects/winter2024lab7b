public enum Suit {
    HEARTS(0.3),
    SPADES(0.2),
    DIAMONDS(0.1),
    CLUBS(0.0);
    private double score;
    Suit(double score){
        this.score= score;
    }
    public double getScore(){
        return this.score;
    }
}
